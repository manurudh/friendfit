class StaticPagesController < ApplicationController
  def home
  	if logged_in?
      @workout = current_user.workouts.build
      @workout_feed_items = current_user.workout_feed.paginate(page: params[:page])
    else
      @workout_feed_items = []
      render 'static_pages/home'
  	end	
  end

  def find_workout
    if logged_in?
      @workout = current_user.workouts.build
      @workout_feed_items = current_user.workout_feed.paginate(page: params[:page])
    else
      @workout_feed_items = []
      render 'static_pages/about'
    end 
    
  end

  def contact
  end
  
end
