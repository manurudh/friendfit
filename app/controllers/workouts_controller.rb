class WorkoutsController < ApplicationController
  
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user,   only: :destroy

  def create
  	@workout = current_user.workouts.build(workout_params)
  	if @workout.save
  		flash[:success] = "Workout created!"
			redirect_to about_url
		else
			render 'static_pages/about'
		end
  end

  def destroy
  	@workout.destroy
  	flash[:success] = "Workout deleted"
    redirect_to request.referrer || about_url

  end

  private
	  def workout_params
	  	params.require(:workout).permit(:activity, :location, :activity_time)
	 	end

	 	def correct_user
      @workout = current_user.workouts.find_by(id: params[:id])
      redirect_to about_url if @workout.nil?
    end

end
