class Workout < ActiveRecord::Base

	belongs_to :user

	# Saves lat & lng to the workout model and sets default units.
  before_save :geocode_location
  acts_as_mappable default_units: :miles,
                   default_formula: :sphere

  validates :location, presence: true
  validates :activity, presence: true
  validates :activity_time, presence: true


  private

  	def geocode_location
      geo = Geokit::Geocoders::GoogleGeocoder.geocode(location)
      self.lat = geo.lat
      self.lng = geo.lng
    end
end
