class CreateWorkouts < ActiveRecord::Migration
  def change
    create_table :workouts do |t|
      t.string :activity
      t.datetime :activity_time
      t.string :location
      t.float :lat
      t.float :lng
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :workouts, [:lat, :lng, :activity_time]

  end
end
