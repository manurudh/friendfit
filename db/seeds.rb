# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Setup
geo = Geokit::Geocoders::GoogleGeocoder.geocode("Cary, NC")
lat = geo.lat
lng = geo.lng

# Users
User.create!(name:  "Anirudh",
             email: "a@example.org",
             password:              "oooooo",
             password_confirmation: "oooooo",
             admin:     true,
             geography: "Cary, NC",
             lat: lat,
             lng: lng,
             activated: true,
             activated_at: Time.zone.now)

50.times do |n|
  name  = Faker::Name.name
  email = "ex-#{n+1}@example.org"
  password = "password"
  User.create!(name: name,
              email: email,
              password:              password,
              password_confirmation: password,
              geography: "Cary, NC",
              lat: lat,
              lng: lng,
              activated: true,
              activated_at: Time.zone.now)
end


# Workouts

users = User.order(:created_at).take(6)
30.times do
  activity = Faker::Lorem.sentence(5)
  activity_time = Faker::Time.forward(10.days)
  users.each do |user| 
    user.workouts.create!(activity: activity, activity_time: activity_time, 
      location: "Cary, NC", lat: lat, lng: lng)
  end
end

# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }